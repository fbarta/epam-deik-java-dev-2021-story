package com.epam.training.webshop.domain.warehouse;

import java.util.List;

import com.epam.training.webshop.domain.order.Product;

public interface Warehouse {
    void registerOrderedProducts(List<Product> products);
}
