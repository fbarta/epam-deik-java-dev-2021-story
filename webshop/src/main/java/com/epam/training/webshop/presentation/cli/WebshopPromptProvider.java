package com.epam.training.webshop.presentation.cli;

import org.jline.utils.AttributedString;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

@Component
public class WebshopPromptProvider implements PromptProvider {
    @Override
    public AttributedString getPrompt() {
        return AttributedString.fromAnsi("Webshop# ");
    }
}
