package com.epam.training.webshop.domain.warehouse;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.epam.training.webshop.domain.order.Basket;
import com.epam.training.webshop.domain.order.Observer;
import com.epam.training.webshop.domain.order.Product;

@Service
public class DummyWarehouse implements Warehouse, Observer {

    private final static Logger LOGGER = LoggerFactory.getLogger(DummyWarehouse.class);

    @Override
    public void registerOrderedProducts(List<Product> products) {
        LOGGER.info("I have registered the order of products {}", products);
    }

    @Override
    public void notify(Basket basket) {
        this.registerOrderedProducts(basket.getProductsFromBasket());
    }
}
