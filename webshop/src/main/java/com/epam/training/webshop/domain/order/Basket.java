package com.epam.training.webshop.domain.order;

import java.util.List;

public interface Basket extends Observable {

    void addProduct(Product product);

    List<Product> getProductsFromBasket();

    void removeProduct(Product productToAdd);

    void addCoupon(Coupon coupon);

    List<Coupon> getCouponsFromBasket();

    double getTotalValue();

    Order order();
}
