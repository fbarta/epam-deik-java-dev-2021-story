package com.epam.training.webshop.configuration;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.training.webshop.domain.order.Basket;
import com.epam.training.webshop.domain.order.BasketImpl;
import com.epam.training.webshop.domain.order.Observer;
import com.epam.training.webshop.repository.OrderRepository;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public Basket basketImpl(OrderRepository orderRepository, List<Observer> observers) {
        Basket basket = new BasketImpl(orderRepository);
        observers.forEach(basket::subscribe);
        return basket;
    }
}
