package com.epam.training.webshop.domain.order;

public interface Product {
    String getName();

    double getValue();
}
