package com.epam.training.webshop.domain.orderconfirm.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.epam.training.webshop.domain.order.Basket;
import com.epam.training.webshop.domain.order.Observer;
import com.epam.training.webshop.domain.orderconfirm.OrderConfirmationService;

@Service
public class DummyOrderConfirmationService implements OrderConfirmationService, Observer {

    private final static Logger LOGGER = LoggerFactory.getLogger(DummyOrderConfirmationService.class);

    @Override
    public void sendOrderConfirmation(Basket basket) {
        LOGGER.info("An order confirmation for basket {} had been sent.", basket.toString());
    }

    @Override
    public void notify(Basket basket) {
        sendOrderConfirmation(basket);
    }
}
