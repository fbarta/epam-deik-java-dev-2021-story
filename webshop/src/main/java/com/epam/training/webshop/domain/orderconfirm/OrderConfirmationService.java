package com.epam.training.webshop.domain.orderconfirm;

import com.epam.training.webshop.domain.order.Basket;

public interface OrderConfirmationService {
    void sendOrderConfirmation(Basket basket);
}
