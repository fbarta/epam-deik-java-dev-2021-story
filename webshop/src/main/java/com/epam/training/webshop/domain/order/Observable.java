package com.epam.training.webshop.domain.order;

public interface Observable {
    void subscribe(Observer observer);
}
